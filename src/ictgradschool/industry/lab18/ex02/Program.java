package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private Canvas canvas;

//    //Instance variable for directions
//    private final int LEFT = 37;
//    private final int RIGHT = 39;
//    private final int DOWN = 40;
//    private final int UP = 38;
//    //
//    private static final Random random = new Random();
//    private int greenDotX = -1, greenDotY = -1;
////    private int[] fPoints =
//    private ArrayList<Integer> redDotXs = new ArrayList<>();
//    private ArrayList<Integer> snakeXs = new ArrayList<>();
//    private ArrayList<Integer> redDotYs = new ArrayList<>();
//    private ArrayList<Integer> snakeYs = new ArrayList<>();
//
//    private int direction;
//    private boolean gameOver = false;

    public static void main(String[] args) {
        //new Program().go();
        Program program = new Program();
        program.setVisible(true);
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//
//
//
//            }
//        });
        program.canvas.go();

    }

    public Program() {

        //JFrame
        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //for every single block, dimension 25 * 25
        //30 blocks in one row, 20 blocks in one column
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
//        this.setPreferredSize(new Dimension(30 * 25 + 6, 20 * 25 + 28));
        setResizable(true);
        System.out.println("before creating a new Canvas");
        this.canvas = new Canvas(this);
        //this.canvas.setPreferredSize(new Dimension(700, 500));
        System.out.println("after creating a new Canvas");

        Container visibleArea = getContentPane();
        visibleArea.add(canvas);

        System.out.println("after adding canvas to visibleArea");
        canvas.setBackground(Color.white);

        add(BorderLayout.CENTER, canvas);
        System.out.println("after adding canvas to JFrame");

        pack();
        canvas.requestFocusInWindow();


    }


    }

/*    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < snakeXs.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(snakeXs.get(i) * 25 + 1, snakeYs.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            g.setColor(Color.green);
            g.fill3DRect(greenDotX * 25 + 1, greenDotY * 25 + 1, 25 - 2, 25 - 2, true);
            for (int i = 0; i < redDotXs.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(redDotXs.get(i) * 25 + 1, redDotYs.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            if (gameOver) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }*/
